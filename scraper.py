from collections import defaultdict
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from xlwt import Workbook

# Take input regarding what city to scrape for
place = input()

# Formulate the URL
query = 'Things to do in ' + place
query_for_url = query.replace(' ', '+')
url = 'https://www.google.com/maps/search/' + query_for_url

# List of stuff to scrape for
stuff_to_fetch_and_write = ['title', 'description', 'rating', 'place_url', 'reviews', 'location_type', 'editorial_quote',
                            'formatted_address', 'has_secondary_data', 'days_and_timings', 'website', 'phone', 'image',
                            'one_line_reviews', 'open_till', 'g_plus_code', 'lat', 'lng']
data = defaultdict(dict)

# Selenium Setup
browser = webdriver.Firefox()
browser.implicitly_wait(10)

# Make the initial request.
browser.get(url)
WebDriverWait(browser, 5).until(EC.title_contains("Things to do in"))

# Start scraping for the results.
result_list = browser.find_elements_by_class_name('section-result')

records = len(result_list)
i=0

while i<records:
    ele = result_list[i]

    activity = ele.find_element_by_class_name('section-result-content')
    browser.execute_script("arguments[0].scrollIntoView(true);", activity);
    title = activity.find_element_by_class_name('section-result-title').text

    # Don't compute more results if we had already scrapped this activity.
    if title in data.keys():
        i+=1
        continue
    print('Fetching for ' + title + '......')
    rating = activity.find_element_by_class_name('section-result-rating').text
    reviews = activity.find_element_by_class_name('section-result-num-ratings').text[1:][:-1]
    description = activity.find_element_by_class_name('section-result-description').text

    # Trigger a click to go to details of an activity.
    try:
        activity.click()
        WebDriverWait(browser, 5).until(EC.url_changes(url))
    except:
        activity.click()
        WebDriverWait(browser, 5).until(EC.url_changes(url))
    # Scrape for activity details.
    place_url = str(browser.current_url)

    def call_with_exception_handling(func, param):
        try:
            return func(param).text
        except NoSuchElementException:
            return 'NA'

    try:
        location_type = browser.find_element_by_class_name('section-hero-header-description-container').text.split('\n')[1]
    except NoSuchElementException:
        location_type = 'NA'

    editorial_quote = call_with_exception_handling(browser.find_element_by_class_name, 'section-editorial-quote')
    formatted_address = call_with_exception_handling(browser.find_element_by_xpath, '//div[@data-section-id="ad"]')
    g_plus_code = call_with_exception_handling(browser.find_element_by_xpath, '//div[@data-section-id="ol"]')
    website = call_with_exception_handling(browser.find_element_by_xpath, '//div[@data-section-id="ap"]')
    phone = call_with_exception_handling(browser.find_element_by_xpath, '//div[@data-section-id="pn0"]')

    try:
        image = browser.find_element_by_class_name('section-hero-header-hero').find_element_by_tag_name('img').get_attribute('src')
    except NoSuchElementException:
        image = 'NA'

    try:
        one_line_reviews = '\n'.join([x.text for x in browser.find_elements_by_class_name('section-review-snippet-line')]).replace('\n\n', '\n')
    except NoSuchElementException:
        one_line_reviews = 'NA'

    try:
        browser.find_element_by_class_name('section-open-hours-button').click()
    except NoSuchElementException:
        timings = 'NA'

    timings = call_with_exception_handling(browser.find_element_by_class_name, 'section-open-hours')
    if timings != 'NA':
        open_till = timings.split('\n')[0]
        if not open_till.startswith('Open until'):
            open_till = 'NA'
        days_and_timings = timings.split('\n')[1:]
        dnt = []
        gap = 0
        for x in range(0, len(days_and_timings)//2):
            day_and_time = days_and_timings[x+gap] + ' ' + days_and_timings[x+gap+1]
            if str.isdigit(day_and_time[0]):
                dnt[-1] = dnt[-1] + ' ' + day_and_time
            else:
                dnt.append(day_and_time)
            gap+=1
        days_and_timings = '\n'.join(dnt)
    else:
        open_till = 'NA'
        days_and_timings = 'NA'

    cords = place_url.split('@')[1].split('/')[0].split(',')
    lat = cords[0]
    lng = cords[1]

    has_secondary_data = 'TRUE'
    if (editorial_quote == 'NA' and
        formatted_address == 'NA' and
        g_plus_code == 'NA'and
        website == 'NA' and
        phone == 'NA' and
        one_line_reviews == 'NA' and
        open_till == 'NA' and
        days_and_timings == 'NA'):
        has_secondary_data = 'FALSE'

    # Return to original url and query for new activities.
    browser.get(url)
    WebDriverWait(browser, 5).until(EC.title_contains("Things to do in"))

    # Setup for new activity
    result_list = browser.find_elements_by_class_name('section-result')
    records = len(result_list)
    i=0

    # Feed the already scrapped data to a higher scope data tracker.
    data[title] = {
        'title': title,
        'description': description,
        'rating': rating,
        'place_url': place_url,
        'reviews': reviews,
        'location_type': location_type,
        'editorial_quote': editorial_quote,
        'formatted_address': formatted_address,
        'has_secondary_data': has_secondary_data,
        'days_and_timings': days_and_timings,
        'website': website,
        'phone': phone,
        'image': image,
        'one_line_reviews': one_line_reviews,
        'open_till': open_till,
        'g_plus_code': g_plus_code,
        'lat': lat,
        'lng': lng,
    }

##### Writing to sheet #######

wb = Workbook()
sheet1 = wb.add_sheet('Sheet 1')

for i, label in enumerate(stuff_to_fetch_and_write):
    sheet1.write(0, i, label)

count = 1
for ele, d in data.items():
    i=0
    for k, v in d.items():
        sheet1.write(count, i, v)
        i+=1
    count+=1

wb.save(place + '.xls')
browser.quit()
